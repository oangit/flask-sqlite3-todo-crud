"""Simple demonstration of a crud app built with flask, sqlite and using the ORM provided by SQLAlchemy"""
import time
from flask import Flask, Response, request
from flask import render_template, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from prometheus_client import generate_latest, Counter, Histogram
from prometheus_client.exposition import CONTENT_TYPE_LATEST

app = Flask(__name__)

# Prometheus metrics
REQUEST_COUNT = Counter(
    'app_request_count', 'App Request Count',
    ['method', 'endpoint', 'http_status']
)
REQUEST_LATENCY = Histogram(
    'app_request_latency_seconds', 'Request latency',
    ['method', 'endpoint']
)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///todo.db'
db = SQLAlchemy(app)

class Todo(db.Model): # pylint: disable=too-few-public-methods
    """ This Todo class """
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(200))
    complete = db.Column(db.Boolean)

@app.before_request
def before_request():
    ''' In the before_request function, we store the start time of the request. '''
    request.start_time = time.time()

@app.after_request
def after_request(response):
    '''In the after_request function, we calculate the request latency,
    record it in the REQUEST_LATENCY histogram, and increment the REQUEST_COUNT counter.'''
    request_latency = time.time() - request.start_time
    REQUEST_LATENCY.labels(request.method, request.path).observe(request_latency)
    REQUEST_COUNT.labels(request.method, request.path, response.status_code).inc()
    return response

@app.route("/")
def index():
    """ TODO doc here """
    todos = Todo.query.all()
    return render_template("index.html", todos=todos)

@app.route('/add', methods=["POST"])
def add():
    """ TODO doc here """
    data = request.form["todo_item"]
    todo = Todo(text=data, complete=False)
    db.session.add(todo)
    db.session.commit()
    # return data # DEBUG REQUEST
    return redirect(url_for("index"))

@app.route('/update', methods=["POST"])
def update():
    """return request.form DEBUG REQUEST"""
    return redirect(url_for("index"))

@app.route('/metrics')
def metrics():
    ''' These metrics will be available in Prometheus format at the /metrics endpoint. '''
    return Response(generate_latest(), mimetype=CONTENT_TYPE_LATEST)

if __name__ == '__main__':
    with app.app_context():
        db.create_all()
        app.run(debug=True, host='0.0.0.0', port=5000)
