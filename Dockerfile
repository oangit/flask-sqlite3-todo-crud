FROM python:3.9-alpine

# Установка необходимых пакетов
RUN apk update && apk add git

# Установка Python и pip
RUN apk add --no-cache python3
RUN python3 -m ensurepip --upgrade

# Копирование файлов приложения
COPY . /app

# Установка зависимостей
WORKDIR /app
RUN pip install -r requirements.txt

# Запуск приложения
CMD ["python", "app.py"]