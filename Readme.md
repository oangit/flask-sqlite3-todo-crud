# An example of a simple todo application
Simple demonstration of a crud app built with flask, sqlite and using the ORM provided by SQLAlchemy.

## How to run
* Clone repo git clone https://gitlab.com/oangit/flask-sqlite3-todo-crud.git
* Go to clone repo cd ./flask-sqlite3-todo-crud
* Creation of virtual environments is done by executing the command venv:
```bash
python3 -m venv venv
```
* A virtual environment may be “activated” using a script in its binary directory (bin on POSIX)
```bash
source ./venv/bin/activate
```
* Install requirements
```bash
python -m pip install -r requirements.txt
```
* Start app
```
./venv/bin/python ./app.py
```

## Prometheus metrics
Additionally, prometheus metrics have been added. These metrics will be available in Prometheus format at the endpoint /metrics.
